<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/cerros_style.css" type="text/css"/>

<?php 
/*
	Template Name: Que Hacer
*/
?>


<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<section id="primary" class="content-area px-0 w-100">
    <main id="main" class="site-main" role="main">
        <div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">
            <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
            <div class="post-thumbnail d-none d-md-block d-xl-none" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />						
            </div>
            <div class="post-thumbnail d-none d-xl-block" style="display: inline-block; position: relative; width: 100%; max-height: 550px;overflow: hidden;">
                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
            </div>
            <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
            </div>
        </div>
        
        <?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
        <div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/que-hacer.jpg" style="width: 100%; h-75;" />	
        </div>
        <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/que-hacer.jpg" style="width: 100%" />
        </div>
        </div>
        </div><!-- #post-## -->

    <div class="container">
        <div class="row justify-content-center">
            <h3 align="center"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;" >¿QUÉ HACER EN EL PARQUE?</b></h3>
            <br><br>
            <p>
            <div class="pl-5">
                <!--funciona el centrado de imagen-->
                <div class="row justify-content-md-center" style="width: 100%; margin-center: 0;"> 
                    <div class="bg-info text-white" style="width: 1100px;">
                        <a align="center" href="<?php echo esc_url( home_url( '/' )); ?>">
                            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/MAPA-VF_web.jpg" tyle="background-image:url" align="right" 											 alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>

</div>
</div>

<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
        <?php
            get_sidebar();
                echo '</div>';
                echo '</main>';
                echo '</section>';
            get_footer();?>
    </b>
</font>