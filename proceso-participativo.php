<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/cerros_style.css" type="text/css"/>

<?php 
/*
	Template Name: Proceso Participativo
*/
?>

<?php get_header(); ?>

<main id="main" class="site-main" role="main">
    <div id="post-384" class="post-384 post type-post status-publish format-standard has-post-thumbnail hentry category-noticias" style="margin-bottom: 1em">
        <div>					
            <div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
                <img src="http://parque.renca.cl/wp-content/uploads/2018/11/landing-02.png" style="width: 100%">
            </div>
            <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                <img src="http://parque.renca.cl/wp-content/uploads/2018/11/landing-02.png" style="width: 100%">
            </div>
        </div>
        <div style="display: none">					
            <div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; height: 400px;overflow: hidden;">
            </div>
            <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
            </div>
        </div>
    </div>
    <div class="row justify-content-md-center" style="width: 100%">
        <div id="content" class="col-sm-12 col-md-12 col-lg-12 col-xl-12" style="font-family: 'Source Sans Pro', sans-serif; color: #000000;">
            <div class="row justify-content-md-center" style="font-family: 'Source Sans Pro', sans-serif; color: #000000;">							
                <div class="col align-self-center">
                    <div id="post-384" class="post-384 post type-post status-publish format-standard has-post-thumbnail hentry category-noticias" style="margin-bottom: 1em">
                        <br>
                        <br>
                        <div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; width:100%; max-height:600px; overflow:hidden;">
                        </div>
                    </div>
                    <div class="row justify-content-md-center" style="width: 100%">
                        <div id="content" class="col-sm-12 col-md-12 col-lg-8 col-xl-9">
                            <div class="mx-4 my-2 px-4">
                                <header class="entry-header">
                                    <h1 id="titulo_noticia" class="entry-title">Así hemos vivido el proceso participativo del Parque Metropolitano Cerros de Renca			</h1>
                                    <br>
                                    <br>
                                    <div id="contenido_noticia" class="entry-content">
                                        Desde marzo del año 2018 como Municipalidad de Renca, junto a Fundación de Urbanismo Social, llevamos adelante un proceso de participación para definir de la mano de la comunidad cuál será el Plan Maestro del Parque Metropolitano Cerros de Renca.

                                        Para esta tarea se ha invitado a participar al arquitecto Teodoro Fernández y el equipo de Lyon Bosch + Martic. Estos destacados profesionales recogerán los resultados de los distintas instancias participativas realizadas con la comunidad para entregar la carta de navegación que contendrá los proyectos detonantes del parque de aquí a 30 años.

                                        <strong>El proceso aún no acaba, todavía puedes contestar las consultas que haremos en nuestras redes sociales y participar de la fiesta de cierre en la que se entregará el Plan Maestro ¡No te quedes fuera, súbete al cerro!</strong>
                                        &nbsp;
                                        <br>
                                        <br>

                                        <h3>El Parque Metropolitano Cerros de Renca se crea con participación</h3>
                                        <br>
                                        <div class="row justify-content-md-center">
                                            <div>
                                                <h4>Revisa todas las etapas de este proceso a continuación:</h4>
                                            </div>
                                            <img align="middle" data-attachment-id="385" data-permalink="http://parque.renca.cl/asi-hemos-vivido-el-proceso-participativo-del-parque-metropolitano-cerros-de-renca/participacion-en-renca-02/" data-orig-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02.jpg" data-orig-size="2118,2118" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="participacion en Renca-02" data-image-description="" data-medium-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg" data-large-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" class="alignnone wp-image-385 size-large jetpack-lazy-image jetpack-lazy-image--handled" src="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" alt="" data-lazy-loaded="1" srcset="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg 1024w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-150x150.jpg 150w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg 300w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-768x768.jpg 768w" sizes="(max-width: 1024px) 100vw, 1024px" width="1024" height="1024"><noscript><img data-attachment-id="385" data-permalink="http://parque.renca.cl/asi-hemos-vivido-el-proceso-participativo-del-parque-metropolitano-cerros-de-renca/participacion-en-renca-02/" data-orig-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02.jpg" data-orig-size="2118,2118" data-comments-opened="0" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="participacion en Renca-02" data-image-description="" data-medium-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg" data-large-file="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" class="alignnone wp-image-385 size-large" src="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg" alt="" width="1024" height="1024" srcset="http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-1024x1024.jpg 1024w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-150x150.jpg 150w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-300x300.jpg 300w, http://parque.renca.cl/wp-content/uploads/2018/11/participacion-en-Renca-02-768x768.jpg 768w" sizes="(max-width: 1024px) 100vw, 1024px" /></noscript>	
                                        </div>
                                </header>
                            </div>	
                        </div>	
                    </div>	
                    <!-- .entry-content -->


<style>
#titulo_noticia{
    font-family:'Source Sans pro';
    font-weight:700;
    size:2.5rem;
    line-height:1.2;
    color:#769E30;
}

#contenido_noticia{
    font-family:'Arial';
    font-weight:400;
    font-size: 1rem;
    line-height:1.5;
    color:#404040;
}
</style>
                    </div>			
                </div>	
            </div>
        </div>
    </div>
</main>	




<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
    <?php get_footer();?>
    </b>
</font>