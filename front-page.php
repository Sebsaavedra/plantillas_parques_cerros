<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/cerros_style.css" type="text/css"/>

<?php 
/*
Template Name: Front Page
*/
?>


<?php get_header(); ?>

<body onload="carga()">
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <div id="primary" class="site-content">
    </div><!-- #primary -->

    <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.11&appId=1841972096058534';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
        </script>



    <section id="video">
        <div id="video_div" class="v-container">
            <iframe width="560" height="215" src="https://www.youtube.com/embed/KiH6GLyLH0A?version=3&loop=1&autoplay=1&cc_load_policy=1rel=0&amp;controls=0&amp;showinfo=0&playlist=U086kasC2fk&mute=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
        </div> 
    </section>

<style>
    @media screen and (max-width: 600px) {
        #video{
            z-index:0; !important;
        }

        #video_div{
            z-index:0; !important;
        }

    }
</style>


<section id="sd" class="content-area px-0 mw-100">
    <main id="main" class="site-main" role="main"></main>



    <!--contador cambiado a justificado-->
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="background: linear-gradient(to right, rgba(73,155,234,1) 0%, rgba(32,124,229,1) 100%); witdh:100%;"><br>
                <h6 class="text-white" align="center" style="font-family: 'Source Sans Pro', sans-serif;">10 MIL ÁRBOLES PARA RENCA</h6><br>
                <p align="center" style="font-family: 'Source Sans Pro', sans-serif;">Trabajamos para convertir nuestros cerros en el futuro Parque Metropolitano Cerros de Renca</p><br>
                <h6 class="text-white" align="center" style="font-family: 'Source Sans Pro', sans-serif;">YA VAMOS EN:</h6>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="row bg-light text-dark" style="border-radius: 20px;">
                            <div class="col-md-auto">  
                                <div>
                                    <font size="7">
                                        <b style="font-family: 'Orbitron', sans-serif; h-100 "><span id="raiz">0</span></b>
                                    </font>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div><!--fin div col-12-->
                <br> 
            </div><!--Fin div row justificado-->
        </div><!--fin div container-->
        <!--Fin cambio de contador justificado-->


        <!--NOTICIAS EN CAJAS-->
        <?php
            // The Query
            $the_query = new WP_Query( array( 'category_name' => 'Destacada' ) );
            $count=0;
            $band=0;
            $uno=1;
            $dos=2;
            if ( $the_query->have_posts() ) { 
                $count = $the_query->post_count;
                $array = array();// The Loop
                for ($i = 1; $i <= $count; $i++) {
                    $the_query->the_post();
                    $address_post_id = get_the_ID() ;
                    $array[$band] = $address_post_id;
                    $penultimo = $count -$uno ;
                    $ultimo = $count - $dos;
                    $band ++;
                }
        ?>
        
        <!-- INICIO CAJAS WEB -->
        <!-- BOX WEB #1 -->
        <section id=bloques_web class="row" style="margin:0 !important;">
            <div class="row">
                <div class="col-md-8" style="background: linear-gradient(to right, rgba(255,175,75,1) 0%, rgba(214,88,39,1) 100%);">
                    <div class="text-white"><br/>
                        <div class="row justify-content-center">
                            <div class="col-md-2">
                                <img src="<?php echo esc_url(home_url( '/' ));?>wp-content/uploads/2018/08/iconos-noticia-1.png" style="max-height:100px" alt="<?php echo esc_attr( get_bloginfo( 'name' ));?>">
                            </div>
                            <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                                <?php echo	'<h4>' . get_the_title($array[$ultimo]) . '</h4>'  ; ?>
                            </div>
                            <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif;">
                                <?php echo '<p style="font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>'	. get_the_excerpt($array[$ultimo]) .'</p>' ;?>
                                <div align="right">
                                    <?php 
                                        echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$ultimo]) . ' 										role="button">';
                                        echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                                        echo	'</a>';
                                    ?>
                                </div>
                            </div> 
                            <br/>
                        </div> 
                        <br/>
                    </div> 
                </div> 
                <br/>
                <?php 
                $url1 = wp_get_attachment_url( get_post_thumbnail_id($array[$ultimo]) );
                echo'<div class="col-md-4" style="background-image:url(' . $url1 . '); background-size:cover; background-position:center;  background-repeat: no-repeat; ">';
                ?>
            </div>

        <!-- BOX WEB #2 -->
            <?php 
                $url = wp_get_attachment_url( get_post_thumbnail_id($array[$penultimo]) );
                echo'<div class="col-md-4" style="background-image:url(' . $url . '); background-size:cover; background-position:center;  background-repeat: no-repeat;">';
                echo '</div>'
            ?>		
            <br>
            <div class="col-md-8" style="background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);">
                <div class="text-white"><br/> 
                    <div class="row justify-content-center">
                        <div class="col-md-2">
                            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/iconos-noticia-2.png" style="max-height:100px " alt="<?php echo esc_attr( get_bloginfo( 'name' ) );?>">
                        </div>
                        <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                            <?php echo	'<h4>' . get_the_title($array[$penultimo]) . '</h4>'  ; ?>
                        </div>
                        <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif;">
                            <?php echo '<p style="font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>'	. get_the_excerpt($array[$penultimo]) .'</p>' ;?>
                            <div align="right">	
                                <?php 
                                    echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$penultimo]) . ' 										role="button">';
                                    echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                                    echo	'</a>';
                                ?>
                            </div>
                        </div> 
                        <br/>
                    </div> 
                    <br/>
                </div> 
            </div>
        </section>
        <!-- FIN CAJAS WEB-->



        <!-- INICIO CAJAS MOBILE -->
        <!-- BOX MOBILE #1 -->
        <section id=bloques_mobile class="row justify-content-center" style="display:none; margin:0 !important;">
            <TABLE>
                <TR>
                    <TD>
                        <div class="col-md-8" style="background: linear-gradient(to right, rgba(255,175,75,1) 0%, rgba(214,88,39,1) 100%);">
                            <div class="text-white"><br/>
                                <div class="row justify-content-center">
                                    <div class="col-md-2">
                                        <img id="icon1" src="<?php echo esc_url(home_url( '/' ));?>wp-content/uploads/2018/08/iconos-noticia-1.png" style="max-height:100px" alt="<?php echo esc_attr( get_bloginfo( 'name' ));?>">
                                    </div>
                                    <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                                        <?php echo	'<h4>' . get_the_title($array[$ultimo]) . '</h4>'  ; ?>
                                    </div>
                                    <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif;">
                                        <?php echo '<p style="font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>'	. get_the_excerpt($array[$ultimo]) .'</p>' ;?>
                                        <div align="right">
                                            <?php 
                                                echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$ultimo]) . ' 										role="button">';
                                                echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                                                echo	'</a>';
                                                                            ?>
                                        </div>
                                    </div> 
                                    <br/>
                                </div> 
                                <br/>
                            </div> 
                        </div> 
                    </TD> 
                </TR>
                
        <!-- BOX MOBILE #2 -->
                <TR>
                    <TD>
                        <div class="col-md-8" style="background: linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%);">
                            <div class="text-white"><br/> 
                                <div class="row justify-content-center">
                                    <div class="col-md-2">
                                        <img id="icon2" src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/iconos-noticia-2.png" style="max-height:100px " alt="<?php echo esc_attr( get_bloginfo( 'name' ) );?>">
                                    </div>
                                    <div class="col-md-8" style="font-family: 'Source Sans Pro', sans-serif;">
                                        <?php echo	'<h4>' . get_the_title($array[$penultimo]) . '</h4>'  ; ?>
                                    </div>
                                    <div class="col-md-10" style="font-family: 'Source Sans Pro', sans-serif;">
                                        <?php echo '<p style="font-family:Source Sans Pro, sans-serif; font-weight:400; line-height:1.2em"><br/>'	. get_the_excerpt($array[$penultimo]) .'</p>' ;?>
                                        <div align="right">	
                                            <?php 
                                                echo '<a class="btn btn-primary; btn btn-outline-light justify-content-end" style="font-size: 16px;" href=' . get_permalink($array[$penultimo]) . ' 										role="button">';
                                                echo	'<b style="text-align: center; font-family: "Source Sans Pro", sans-serif; font-size: 16px;">Ver más...</b>';
                                                echo	'</a>';
                                            ?>
                                        </div>
                                    </div> 
                                    <br/>
                                </div> 
                                <br/>
                            </div> 
                        </div>
                    </TD> 
                </TR>
            </TABLE>
        </section>
        <!-- FIN CAJAS MOBILE-->
<?php
}
?> 


        <!--funciona el centrado de imagen-->
        <div class="container"><br>
            <h3 align="center"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">MAPA CERROS DE RENCA</b></h3><br>
        </div>
        <div class="row justify-content-md-center" style="width: 100%; margin-center: 0;"> 
            <div class="bg-info text-white" style="width: 1100px;">
                <a href="<?php echo esc_url( home_url( '/' )); ?>">
                    <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/MAPA-VF_web.jpg" tyle="background-image:url" align="right" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                </a>
            </div>
            <br>
            <div id=titulo_colaboradores class="container"><br><br>
                <h3 align="left"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">COLABORADORES</b></h3><br>
            </div>
            <div class="row" id=colaboradores>
                <div class="col-md-3"><a href="http://parque.renca.cl/">
                    <img src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-cultiva-1.png" style="max-width: 200%; max-height: 80%;  " align="right" alt="Parque Metropolitano Cerros de Renca">
                    </a>
                </div>
                <div class="col-md-3"><a href="http://parque.renca.cl/">
                    <img src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-atquitectos-1.png" style="max-width: 100%; max-height: 100%; margin-top:35px; margin-left:60px;"  alt="Parque Metropolitano Cerros de Renca">
                    </a>
                </div>
                <div class="col-md-3"><a href="http://parque.renca.cl/">
                    <img src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-lbm-2.png" style="max-height: 100%; margin-top:-20px; margin-left:100px;"  alt="Parque Metropolitano Cerros de Renca">
                    </a>
                </div>
                <div class="col-md-3" style="position: left;"><a href="http://parque.renca.cl/">
                    <img align="left" src="http://parque.renca.cl/wp-content/uploads/2018/08/logos-socios-urbanismo-social.png" style="max-height: 80%; margin-top:10px;">
                    </a>
                </div>
            </div>
            <br>
            <br>

<style>
@media screen and (max-width: 600px) {
    #bloques_web{
        display:none !important;
    }

    #bloques_mobile{
        display:block !important;
    }
    
    #contenedor_front{
        padding:0px !important;
    }

    #icon1{
        width:20% !important;
    }
    
    #icon2{
        width:20% !important;
    }
    
    #colaboradores{
        display:none !important;
    }
    
    #titulo_colaboradores{
        display:none !important;
    }

    #tabla_footer{
        display:none !important;
    }
}
</style>		

<script src="jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    var arbolometro;
    function detenerse(){
        clearInterval(arbolometro);
    }
    
    function carga()
    {
        arbol_1 =0;
        s = document.getElementById("raiz");
        arbolometro = setInterval(
    function(){
        if(arbol_1==3622){
        arbol_1=3622;
            if(arbol_1===arbol_1){
                detenerse();
            }
        }
        s.innerHTML = arbol_1;
        arbol_1++;
    }
            ,3);
    }
</script>



<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
    <?php
        get_sidebar();
        get_footer();
    ?>
    </b>
</font>




