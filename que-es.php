<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/cerros_style.css" type="text/css"/>

<?php 
/*
	Template Name: Que es
*/
?>

<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<div id="primary" style="width:100%">
    <div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom:1em">
        <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
        <div class="post-thumbnail d-none d-md-block d-xl-none" style="display:inline-block; position:relative;   max-height: 400px;overflow: hidden;">
            <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />				
        </div>
        <div class="post-thumbnail d-none d-xl-block" style="display:inline-block; position:relative;  max-height:550px; overflow:hidden;">
            <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
        </div>
        <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
            <img src="<?php echo the_post_thumbnail_url(); ?>" style="width:100%" />
        </div>
    </div>

    <div class="card" style="background: linear-gradient(to right, rgba(255,175,75,1) 0%, rgba(214,88,39,1) 100%);">
        <div class="row justify-content-md-center" style="width:100%">
            <div class="col-9">
                <div class="text-white"> 
                    <div class="card-body" style="font-family: 'Source Sans Pro', sans-serif;  text-align: justify;">
                    </div> 
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/icono-cerro.png" style="max-height: 90px">
                            </div>

                            <div class="col-md-7"><br>
                                <h3 align="left">
                                    <b style="font-family: 'Source Sans Pro', sans-serif;">PARQUE METROPOLITANO CERROS DE RENCA</b>
                                </h3> 
                            </div>
                        </div>
                    </div><p>
                    <div class="container">
                        <span class="textocontainer" style="font-family: 'Source Sans Pro'"> 
                            La Municipalidad de Renca posee 207 ha para desarrollar un futuro parque, proyecto que requiere como primer paso 
                            la creación de un “Plan Maestro”.<br> Para esta tarea se ha invitado a participar al arquitecto
                            Teodoro Fernández y el equipo de Lyon Bosch + Martic. Este plan pretende ser la carta de navegación que guíe el 
                            desarrollo del parque para los próximos 30 años, con el fin de ir 
                            concretando de manera paulatina, a través de proyectos detonantes, 
                            el Parque Metropolitano Cerros de Renca, 
                            y así dotar de infraestructura de alto estándar a la principal área verde de la 
                            zona nor-poniente de la capital.
                        </span>
                    </div>   
                    <br><br><br><br>
                </div><!--div justivi-->
            </div>
        </div>
    </div>

    <!-- INICIO MAPA -->
    <div class="row" style="width:100%; margin:0px;">
        <div class="text-white"> 
            <div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; width:100%; max-height:500px; overflow:hidden;">
                <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/Plan-Maestro.jpg" style="width:100%; height:160%;"/>	
            </div>
            <div class="post-thumbnail d-block d-md-none" style="width:100%;">
                <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/Plan-Maestro.jpg" style="width:100%; height:130%;" />
            </div>			 
        </div>
    </div>
    <!-- FIN MAPA -->
    <br><br>
    
    <!--PROYECTO-->

    <div class="container">
        <div class="row justify-content-md-center" style="width:100%">
            <div class="col-md-12" style="background:linear-gradient(to right, rgba(164,179,87,1) 0%, rgba(117,137,12,1) 100%); width:100%"><br>
                <div class="container">	 
                    <div class="text-white">
                        <div class="row">
                            <div class="col-md-2">
                                <img align="right" src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/icono-plano.png" style="max-height:90px">
                            </div>
                            <div class="col-md-7"><br>
                                <h3 align="left"><b style="font-family: 'Source Sans Pro', sans-serif;">EL PROYECTO</b></h3> 
                            </div>
                        </div>
                    </div>
                </div><p>

                <div class="container" style="width:100%">
                    <span class="text-white" style="vertical-align: inherit; font-family: 'Source Sans Pro', sans-serif; width:100%">
                        La primera etapa del Plan Maestro Cerros de Renca contempla la intervención del cerro Renca, denominada Zona A y en una segunda etapa el desarrollo del cerro Colorado, denominada Zona B. Para la Zona A está contemplado el mejoramiento de las obras existentes a través de la conservación de espacios públicos (FNDR), la habilitación de un mirador en la cumbre, planes de forestación, un vivero metropolitano, zona deportiva, zona de las tradiciones y la creación de senderos y miradores.
                        Para la Zona B, está contemplado el hermoseamiento del actual acceso a la cumbre, formalización de senderos, construcción de miradores y forestación.
                    </span>
                    <br/>
                    <p><p><p><p>
                </div>
            </div>
            <p><p>
        </div>
        <br><br/><br/>
    </div>
    </div>
    <br/><br/>
    <!-- FIN PROYECTO -->

    <!--PROYECTO-->

    <div class="container">
        <div class="row justify-content-md-center" style="width:100%">
            <div class="col-md-12" style="color:#FFFF; width:100%"><br>
                <div class="container">	 
                    <div class="row">
                        <div class="col-md-2">
                            <img align="right" src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/para-plan-maestro-02.png" style="max-height:90px">
                        </div>
                        <div class="col-md-7"><br>
                            <h3 align="left"><b style="color:#769E30; font-family:'Source Sans Pro', sans-serif;">¿PARA QUÉ UN PLAN MAESTRO?</b></h3> 
                        </div>
                    </div>
                </div><p>						  
                <div class="container" style="width:100%">
                    <span style="vertical-align:inherit; font-family:'Source Sans Pro', sans-serif; width:100%; color:#201A19;">
                        El objetivo del Plan Maestro es definir el programa y los proyectos detonantes,
                        a partir de procesos participativos con la comunidad, 
                        los cuales estarán a cargo de la fundación Urbanismo Social, 
                        y del reconocimiento de las pre-existencias de los cerros y su entorno.
                        Esto con el fin de crear una cartera de proyectos para mejorar la oferta programática del parque (experiencia del usuario). Al mismo tiempo el plan maestro debe considerar las áreas de reforestación para los Cerros de Renca, con el fin de revertir las condiciones actuales de la vegetación existente.
                    </span>
                    <br><p><p><p><p>
                </div>
            </div>
            <p><p>
        </div>
        <br><br><br>
    </div>
    <!--FIN PROYECTO-->

    <!--PROYECTO DOWN, ahora es responsive-->
    <div class="row" style="width:100%; margin:0px;">
        <div class="post-thumbnail d-none d-md-block" style="display:inline-block; position:relative; width:100%; max-height:500px; overflow:hidden; width:100%;">
            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/el-proyecto-down3.jpg" style="width:100%;" />	
        </div>
        <div class="post-thumbnail d-block d-md-none" >
            <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/el-proyecto-down3.jpg" style="width:100%;" />
        </div>			 			      
    </div>
    <!-- FIN PROYECTO DOWN -->
    </main>
</div>


<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
        <?php
            get_sidebar();
            get_footer(); 
        ?>
    </b>
</font>



