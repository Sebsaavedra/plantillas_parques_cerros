<link rel="stylesheet" href="/wp-content/themes/wp-bootstrap-starter/cerros_style.css" type="text/css"/>

<?php 
/*
Template Name: Como Llegar
*/
?>

<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<section id="primary" class="content-area px-0 w-100 h-71">
<main id="main" class="site-main" role="main">
    <div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">
        <?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
            <div class="post-thumbnail d-none d-md-block d-xl-none" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
            </div>
            <div class="post-thumbnail d-none d-xl-block" style="display: inline-block; position: relative; width: 100%; max-height: 550px;overflow: hidden;">
                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
            </div>
            <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
            </div>
    </div>
        <?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
            <div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
                <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/como-llegar.jpg" style="width: 100%; h-70;" />
            </div>
            <div class="post-thumbnail d-block d-md-none" style="width: 100%;">
                <img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/10/como-llegar.jpg" style="width: 100%" />
            </div>
        </div><!--antes del post-->
</section>

    <div class="row" style="width:100%;">
        <div id="content" class="col-md-12" >

                <div class="container" style="padding-top:-20px;">
                    <h3 align="center"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E30;">¿CÓMO LLEGAR?</b></h3><br>
                </div>
                <div class="container">
                    <label style="font-family: 'Source Sans Pro', sans-serif;  color: #201A19;"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;<b>Acceso Cerro Renca:</b>&nbsp;&nbsp;  Av. El Cerro #1555, Renca.</label>
                    <br>
                    <label style="font-family: 'Source Sans Pro', sans-serif;  color: #201A19; width: 300px;"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;<b>Acceso Cerro Colorado:</b>&nbsp;&nbsp; </label>
                    <br>
                    <label style="font-family: 'Source Sans Pro', sans-serif; color: #201A19; width: 300px;"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;<b>Acceso Mapumahuida:</b>&nbsp;&nbsp; </label>
                    <br>

                <div class="row" style="width:100%; height:500px; position:static !important;">
                    <div class="embed-container">
                         <iframe title="PARQUE METROPOLITANO CERROS DE RENCA" src="//www.arcgis.com/apps/Embed/index.html?webmap=d09cee4a247844c1b27e9a92e5b888e5&amp;extent=-70.7463,-33.4105,-70.6882,-33.3907&amp;zoom=true&amp;previewImage=false&amp;scale=true&amp;details=true&amp;legend=true&amp;active_panel=details&amp;disable_scroll=true&amp;theme=light" width="1110" height="500" frameborder="0" marginwidth="0" marginheight="0" scrolling="no">
                        </iframe>
                    </div>
                </div>				
                </div>
                <br>					
        </div>
    </div>

<style>
    .embed-container{
        position: static; 
        padding-bottom:10%; 
        height:0; 
        max-width:100%;
    } 
    
    .embed-container iframe, .embed-container object, .embed-container iframe{
        position: absolute; 
        top: 0; 
        margin-left:0; 
        margin-top:10%; 
        width: 60%; 
        height: 60%;
    } 
    
    small{
        position:absolute; 
        z-index:80; 
        bottom:0; 
        margin-bottom:-15px;
    }
</style>

<font size=7>
    <b style="font-family: 'Orbitron', sans-serif; h-100 ">
        <?php
        get_sidebar();
        get_footer();
        ?>
    </b>
</font>

