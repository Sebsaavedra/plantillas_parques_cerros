<?php 
/*
	Template Name: Actividades
*/
?>


<?php get_header(); ?>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<section id="primary" class="content-area px-0 w-100">
		<main id="main" class="site-main" role="main">
			<div id="post-<?php the_ID(); ?>" <?php post_class();?> style="margin-bottom: 1em">
				<?php if(has_post_thumbnail()){ echo '<div>';} else{echo '<div style="display: none">';}?>
					<div class="post-thumbnail d-none d-md-block d-xl-none" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
						
					</div>
					<div class="post-thumbnail d-none d-xl-block" style="display: inline-block; position: relative; width: 100%; max-height: 550px;overflow: hidden;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
						
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%" />
					</div>

				</div>
				<?php if(has_post_thumbnail()){ echo '<div style="display: none">';} else{echo '<div>';}?>
					<div class="post-thumbnail d-none d-md-block" style="display: inline-block; position: relative; width: 100%; max-height: 400px;overflow: hidden;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/actividades.jpg" style="width: 100%; h-75;" />
						
					</div>
					<div class="post-thumbnail d-block d-md-none" style="width: 100%;">
						<img src="<?php echo esc_url( home_url( '/' )); ?>wp-content/uploads/2018/08/actividades.jpg" style="width: 100%" />
					</div>
				</div>
			</div><!-- #post-## -->
			
			<div class="row justify-content-md-center" style="width: 100%; margin-center: 0;">
				<div id="content" class="col-md-12">
					<div align="justify"> 
					<h3 align="center"><b style="font-family: 'Source Sans Pro', sans-serif; color: #769E33;">ACTIVIDADES</b></h3><br><br>
					
					<!--collapsive Primer paso -->
	<div class="container">
		<div class="row justify-content-between col-12" style="align:center">
			
			<!-- actividad #1 --> 
			<div id="box" class="col-md-4">
				<img src="http://parque.renca.cl/wp-content/uploads/2018/10/Primer-paso-hacia-el-futuro.jpg" 					border="1" alt="Este es el ejemplo de un texto alternativo" width="400" height="300">
					<h4 id="titulos_actividad">Primer paso hacia el futuro Parque 	Metropolitano Cerros de 					Renca</h4>   
  				<input id="toggle" type="checkbox" unchecked>
  				<label id="label" for="toggle">Ver más</label>
  				<div id="expand">
    				<section>
      					<p id="parrafo"> Con un gran seminario dimos inicio al proyecto 
						  que busca convertir los Cerros de Renca en un 
						  Parque Metropolitano para el sector Norponiente de 
						  Santiago. En el Santuario Laura Vicuña, 
						  participaron autoridades, académicos y representantes 
						  de la comunidad, quienes compartieron su visión sobre 
						  las oportunidades y desafíos de los cerros urbanos. </p>
    				</section>
  				</div>
				<div class="col-md-4">
				</div>
  				<section>
   				</section> 
			</div>
			<!-- fin #1 --> 

		
			<!-- actividad #2 --> 
			<div id="box1" class="col-md-4">
				<img src="http://parque.renca.cl/wp-content/uploads/2018/10/Limpieza-1500-toneladas.png" border="1" alt="Este es el ejemplo de un texto alternativo" width="400" height="300">
					<h4 id="titulos_actividad1">Limpieza de 1.500 toneladas de basura en vertedero ilegal (2017)</h4>   
  				<input id="toggle1" type="checkbox" unchecked>
  				<label id="label1" for="toggle1">Ver más</label>
  				<div id="expand1">
    				<section>
      					<p id="parrafo1"> A los pies del Cerro Renca, por el sector de Apóstol Santiago, 
						se retiraron más de 1500 toneladas de basura, recuperando ese sector público 
						que se había transformado en un vertedero ilegal. Además, constantemente se 
						realizan operativos de limpieza para mantener limpias las hectáreas de los cerros. 
						</p>
    				</section>
  				</div>
				<div class="col-md-4">
				</div>
  				<section>
   				</section> 
			</div>
			<!-- fin #2 --> 
		</div>
		
		<br>
		<br>
		
		<div class="row justify-content-between col-12" style="align:center">
            
			<!-- actividad #3 --> 
			<div id="box2" class="col-md-4">
				<img src="http://parque.renca.cl/wp-content/uploads/2018/10/Jornada-de-limpieza-de-cerro.jpg" border="1" alt="Este es el ejemplo de un texto alternativo" width="400" height="300">
					<h4 id="titulos_actividad2">Jornadas de limpieza del cerro con colegios y voluntarios de CULTIVA, 2017-2018</h4>   
  				<input id="toggle2" type="checkbox" unchecked>
  				<label id="label2" for="toggle2">Ver más</label>
  				<div id="expand2">
    				<section>
      					<p id="parrafo2"> Durante los dos últimos años se han intensificado las 
					jornadas de reforestación organizadas junto a Cultiva, 
					utilizando una modalidad que incluye voluntarios y 
					en muchos casos educación ambiental, junto a escuelas de la comuna. 
					Gracias a estas jornadas y otros esfuerzos hechos por el municipio, 
					actualmente contamos con 1981 nuevos árboles. </p>
    				</section>
  				</div>
				<div class="col-md-4">
				</div>
  				<section>
   				</section> 
			</div>
			<!-- fin #3 --> 

		
			<!-- actividad #4 --> 
			<div id="box3" class="col-md-4">
				<img src="http://parque.renca.cl/wp-content/uploads/2018/10/Inauguración-Sendero.jpg" border="1" alt="Este es el ejemplo de un texto alternativo" width="400" height="300">
					<h4 id="titulos_actividad3">Inauguración Sendero hacia la Cueva de Don Emilio, 2018</h4>   
  				<input id="toggle3" type="checkbox" unchecked>
  				<label id="label3" for="toggle3">Ver más</label>
  				<div id="expand3">
    				<section>
      					<p id="parrafo3"> Durante el año 2017 se adjudicaron una serie de obras relacionadas a la tradicional Cueva de Don Emilio. Se construyó cerca de 1 km 
de sendero y dos miradores de 60 mt2, financiados mediante compensaciones de la Clínica Alemana y ejecutado por la Fundación CULTIVA. A esto se suman 1.200 mts de cortafuegos, en línea con la generación de infraestructura que permita disfrutar del parque.
						</p>
    				</section>
  				</div>
				<div class="col-md-4">
				</div>
  				<section>
   				</section> 
			</div>
			<!-- fin #4 --> 
		</div>	
	</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
						
<!-- Estilo Activdad #0 --> 
<style>
	#parrafo{
		color: black;
		font-size: 17px;
		line-height: 1.3em;
		font-weight: 600;
	}
    
	#toggle{
		  display: none;
  		  visibility: hidden;
	}
    
	#label{
	  display: block;
	  padding: 0.5em;
	  text-align: center;
	  color: #4D4D4D;
	}
    
	label:hover {
	  color: #000;
	}
    
	label::before {
	  font-family: Consolas, monaco, monospace;
	  font-weight: bold;
	  font-size: 15px;
	  content: "˅";
	  vertical-align: text-top;
	  display: inline-block;
	  width: 20px;
	  height: 20px;
	  margin-right: 3px;
	}
    
	#expand {
	  height: 0px;
	  overflow: hidden;
	  transition: height 0.2s;
	  color: #FFF;
      padding: 0px;
	}
    
	section {
	  padding: 0 20px;
	}
    
	#toggle:checked ~ #expand {
	  height: 250px;
	}
	
	#toggle:checked ~ label::before {
	  content: "^";
	}
    
	#titulos_actividad{
		position: relative;
  		color: #769E33;
		padding: 5px;
	}
    
	#box{
		padding: 10px;
		border: 2px solid #769E33;
	}
</style>
<!-- Fin Estilo Activdad #0 --> 
						
										
<!-- Estilo Activdad #1 --> 
<style>
	#parrafo1{
		color: black;
		font-size: 17px;
		line-height: 1.3em;
		font-weight: 600;
	}
    
	#toggle1{
		  display: none;
  		  visibility: hidden;
	}
    
	#label1{
	  display: block;
	  padding: 0.5em;
	  text-align: center;
	 
	  color: #4D4D4D;
	}
    
	label:hover {
	  color: #000;
	}
    
	label::before {
	  font-family: Consolas, monaco, monospace;
	  font-weight: bold;
	  font-size: 15px;
	  content: "˅";
	  vertical-align: text-top;
	  display: inline-block;
	  width: 20px;
	  height: 20px;
	  margin-right: 3px;
	  
	}
	#expand1 {
	  height: 0px;
	  overflow: hidden;
	  transition: height 0.5s;
	  color: #FFF;
      padding: 0px;
	}
    
	section {
	  padding: 0 20px;
	}
    
	#toggle1:checked ~ #expand1 {
	  height: 250px;
	}
    
	#toggle1:checked ~ label::before {
	  content: "^";
	}
    
	#titulos_actividad1{
		position: relative;
  		color: #769E33;
		padding: 5px;
	}
    
	#box1{
		padding: 10px;
		border: 2px solid #769E33;
	}
</style>
<!-- Fin Estilo Activdad #1 --> 
									
						
<!-- Estilo Activdad #2 --> 
<style>
	#parrafo2{
		color: black;
		font-size: 17px;
		line-height: 1.3em;
		font-weight: 600;
	}
    
	#toggle2{
		  display: none;
  		  visibility: hidden;
	}
    
	#label2{
	  display: block;
	  padding: 0.5em;
	  text-align: center;
	  color: #4D4D4D;
	}
    
	label:hover {
	  color: #000;
	}
    
	label::before {
	  font-family: Consolas, monaco, monospace;
	  font-weight: bold;
	  font-size: 15px;
	  content: "˅";
	  vertical-align: text-top;
	  display: inline-block;
	  width: 20px;
	  height: 20px;
	  margin-right: 3px;
	}
    
	#expand2 {
	  height: 0px;
	  overflow: hidden;
	  transition: height 0.5s;
	  color: #FFF;
      padding: 0px;
	}
    
	section {
	  padding: 0 20px;
	}
    
	#toggle2:checked ~ #expand2 {
	  height: 250px;
	}
    
	#toggle2:checked ~ label::before {
	  content: "^";
	}
    
	#titulos_actividad2{
		position: relative;
  		color: #769E33;
		padding: 5px;
	}
    
	#box2{
		padding: 10px;
		border: 2px solid #769E33;
	}
</style>
<!-- Fin Estilo Activdad #2 --> 
					
					
						
<!-- Estilo Activdad #3 --> 
<style>
	#parrafo3{
		color: black;
		font-size: 17px;
		line-height: 1.3em;
		font-weight: 600;
	}
    
	#toggle3{
		  display: none;
  		  visibility: hidden;
	}
    
	#label3{
	  display: block;
	  padding: 0.5em;
	  text-align: center;
	  color: #4D4D4D;
	}
    
	label:hover {
	  color: #000;
	}
    
	label::before {
	  font-family: Consolas, monaco, monospace;
	  font-weight: bold;
	  font-size: 15px;
	  content: "˅";
	  vertical-align: text-top;
	  display: inline-block;
	  width: 20px;
	  height: 20px;
	  margin-right: 3px;
	}
	
	#expand3 {
	  height: 0px;
	  overflow: hidden;
	  transition: height 0.5s;
	  color: #FFF;
      padding: 0px;
	}
    
	section {
	  padding: 0 20px;
	}
    
	#toggle3:checked ~ #expand3 {
	  height: 250px;
	}
	
	#toggle3:checked ~ label::before {
	  content: "^";
	}
	
	#titulos_actividad3{
		position: relative;
  		color: #769E33;
		padding: 5px;
	}
    
	#box3{
		padding: 10px;
		border: 2px solid #769E33;
	}
</style>
<!-- Fin Estilo Activdad #3 --> 

                        
                        
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
						
<!-- Script Actividad #0 --> 				
<script>
$('#toggle').on('change', function() {
    if ($(this).is(':checked') ) {
        console.log("Checkbox " + $(this).prop("#toggle") +  " (" + $(this).val() + ") => Seleccionado");
		$("#box").css("background-image", "linear-gradient(to right,rgb(153, 170,0)0%, rgb(153,170,0)1%, 	rgb(138,164,55)91%, rgb(138,164,55)96%)");
		$("#titulos_actividad").css("color", "white");
		$("#parrafo").css("color", "white");
		$("#toggle").css("color", "white");
		$("#label").css("color", "white");
		$("#box").css("border", "0px solid white;");
		$("#box").css("border-radius:", "25px;");
		$("#box1").css("height", "50%");
		$("#box2").css("height", "50%");
		$("#box3").css("height", "50%");
		$("#label").text("Ver menos");
    } else {
        console.log("Checkbox " + $(this).prop("#toggle") +  " (" + $(this).val() + ") => Deseleccionado");
		$("#box").css("background-color", "white");
		$("#titulos_actividad").css("color", "#769E33");
		$("#box").css("background", "white");
		$("#label").css("color", "#4D4D4D");
		$("#parrafo").css("color", "black");
		$("#label").text("Ver más");
		$("#box").css("height", "50%");
    }
});
</script>						
<!-- Fin Script Actividad #0 --> 
	
<!-- Script Actividad #1 --> 				
<script>
$('#toggle1').on('change', function() {
    if ($(this).is(':checked') ) {
        console.log("Checkbox " + $(this).prop("#toggle1") +  " (" + $(this).val() + ") => Seleccionado");
		$("#box1").css("background-image", "linear-gradient(to right,rgb(153, 170,0)0%, rgb(153,170,0)1%, 	rgb(138,164,55)91%, rgb(138,164,55)96%)");
		$("#titulos_actividad1").css("color", "white");
		$("#parrafo1").css("color", "white");
		$("#toggle1").css("color", "white");
		$("#label1").css("color", "white");
		$("#box1").css("border", "0px solid white;");
		$("#box1").css("border-radius:", "25px;");
		$("#box").css("height", "50%");
		$("#box2").css("height", "50%");
		$("#box3").css("height", "50%");
		$("#label1").text("Ver menos");
    } else {
        console.log("Checkbox " + $(this).prop("#toggle1") +  " (" + $(this).val() + ") => Deseleccionado");
		$("#box1").css("background-color", "white");
		$("#titulos_actividad1").css("color", "#769E33");
		$("#box1").css("background", "white");
		$("#label1").css("color", "#4D4D4D");
		$("#parrafo1").css("color", "black");
		$("#box1").css("height", "50%");
		$("#label1").text("Ver más");
    }
});
</script>						
<!-- Fin Script Actividad #1 --> 

<!-- Script Actividad #2 --> 				
<script>
$('#toggle2').on('change', function() {
    if ($(this).is(':checked') ) {
        console.log("Checkbox " + $(this).prop("#toggle2") +  " (" + $(this).val() + ") => Seleccionado");
		$("#box2").css("background-image", "linear-gradient(to right,rgb(153, 170,0)0%, rgb(153,170,0)1%, 	rgb(138,164,55)91%, rgb(138,164,55)96%)");
		$("#titulos_actividad2").css("color", "white");
		$("#parrafo2").css("color", "white");
		$("#toggle2").css("color", "white");
		$("#label2").css("color", "white");
		$("#box2").css("border", "0px solid white;");
		$("#box2").css("border-radius:", "25px;");
		$("#box").css("height", "50%");
		$("#box1").css("height", "50%");
		$("#box3").css("height", "50%");
		$("#label2").text("Ver menos");	
    } else {
        console.log("Checkbox " + $(this).prop("#toggle2") +  " (" + $(this).val() + ") => Deseleccionado");
		$("#box2").css("background-color", "white");
		$("#titulos_actividad2").css("color", "#769E33");
		$("#box2").css("background", "white");
		$("#label2").css("color", "#4D4D4D");
		$("#parrafo2").css("color", "black");
		$("#box2").css("height", "50%");
		$("#label2").text("Ver más");		
    }
});
</script>						
<!-- Fin Script Actividad #2 --> 						
			

<!-- Script Actividad #3 --> 				
<script>
$('#toggle3').on('change', function() {
    if ($(this).is(':checked') ) {
        console.log("Checkbox " + $(this).prop("#toggle2") +  " (" + $(this).val() + ") => Seleccionado");
		$("#box3").css("background-image", "linear-gradient(to right,rgb(153, 170,0)0%, rgb(153,170,0)1%, 	rgb(138,164,55)91%, rgb(138,164,55)96%)");
		$("#titulos_actividad3").css("color", "white");
		$("#parrafo3").css("color", "white");
		$("#toggle3").css("color", "white");
		$("#label3").css("color", "white");
		$("#box3").css("border", "0px solid white;");
		$("#box3").css("border-radius:", "25px;");
		$("#box").css("height", "50%");
		$("#box1").css("height", "50%");
		$("#box2").css("height", "50%");
		$("#label3").text("Ver menos");
    } else {
        console.log("Checkbox " + $(this).prop("#toggle2") +  " (" + $(this).val() + ") => Deseleccionado");
		$("#box3").css("background-color", "white");
		$("#titulos_actividad3").css("color", "#769E33");
		$("#box3").css("background", "white");
		$("#label3").css("color", "#4D4D4D");
		$("#parrafo3").css("color", "black");
		$("#box3").css("height", "50%");
		$("#label3").text("Ver más");
    }
});
</script>						
<!-- Fin Script Actividad #3 --> 			

						
								
<font size=7>
	<b style="font-family: 'Orbitron', sans-serif; h-100">
	<?php
get_sidebar();
echo '</div>';
echo '</main>';
echo '</section>';
get_footer();?>
	</b>
</font>

